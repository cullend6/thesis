﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TexFlipper
{
    public static Texture2D Flip(Texture2D sourceTex)
    {
        // Create a new Texture2D the same dimensions and format as the input
        Texture2D Output = new Texture2D(sourceTex.width, sourceTex.height, sourceTex.format, false);
        // Loop through pixels
        for (int y = 0; y < sourceTex.height; y++)
        {
            for (int x = 0; x < sourceTex.width; x++)
            {
                // Retrieve pixels in source from left-to-right, bottom-to-top
                Color pix = sourceTex.GetPixel(sourceTex.width + x, (sourceTex.height - 1) - y);
                // Write to output from left-to-right, top-to-bottom
                Output.SetPixel(x, y, pix);
            }
        }
        return Output;
    }
}
