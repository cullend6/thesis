﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetWebCamTex : MonoBehaviour {

    private GameObject cube;

	// Use this for initialization
	void Start () {
        cube = GameObject.Find("Cube");
    }
	
	// Update is called once per frame
	void Update () {
        Renderer renderer = cube.GetComponent<Renderer>();
        Texture im = renderer.material.mainTexture;
        Renderer rend2 = GetComponent<Renderer>();
        rend2.material.mainTexture = im;
    }
}
