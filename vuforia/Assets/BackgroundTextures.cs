﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.Linq;
using UnityEngine.VR.WSA.WebCam;

public class BackgroundTextures : MonoBehaviour {

    // Use this for initialization
    void Start () {
        WebCamTexture wbI = new WebCamTexture();
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = wbI;
        wbI.Play();
    }

    // Update is called once per frame
    void Update () {

    }
}
