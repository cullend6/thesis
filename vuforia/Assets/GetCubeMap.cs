﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetCubeMap : MonoBehaviour {

    private Cubemap Background;
    public GameObject C1;

	// Use this for initialization
	void Start () {
        Background = new Cubemap(128,TextureFormat.RGBA32,true);
	}
	
	// Update is called once per frame
	void Update () {
        // create temporary camera for rendering
        //GameObject cubeObject = GameObject.Find("Camera");
        Camera cubeCam = GetComponent<Camera>();
        cubeCam.RenderToCubemap(Background);

        Debug.Log("RUNNING");

        //GameObject C1 = GameObject.Find("C1");
        Renderer rend = C1.GetComponent<Renderer>();
        rend.material.mainTexture = Background;

        Debug.LogError("tex size : " + rend.material.mainTexture.height);

        

    }
}
