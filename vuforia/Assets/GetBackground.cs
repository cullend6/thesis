﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;



public class GetBackground : MonoBehaviour {

    public GameObject C1;
    public GameObject Target;
    public Cubemap cubemap;
    public Texture2D front;
    public Texture2D top;
    public Texture2D bottom;
    public Texture2D right;
    public Texture2D left;

    // Use this for initialization
    void Start () {
            Cubemap cubemap = new Cubemap(32, TextureFormat.RGBA32, false);
    }
	
	// Update is called once per frame
	void Update () {
        //Check if object is in scene
        if (Target.GetComponent<Vuforia.DefaultTrackableEventHandler>().Tracked)
        {
        
            Renderer rend2 = GetComponent<Renderer>();
            Texture2D im = (Texture2D)rend2.material.mainTexture;
            int pixelSize = 256;
            //Cubemap cubemap = new Cubemap(pixelSize, TextureFormat.RGBA32, false);
            

            //Find top texture for cube map
            Color[] ctop = im.GetPixels(0, 0, im.width, 120);
            //cubemap.SetPixels(ctop, CubemapFace.PositiveY);
            top = new Texture2D(im.width, 120);
            top.SetPixels(ctop);
            top.Apply();
            //top.Resize(128, 128);
            //ctop = top.GetPixels();

            //Find bottom texture for cube map
            Color[] cbottom = im.GetPixels(0, 360, im.width, 120);
            //cubemap.SetPixels(cbottom, CubemapFace.NegativeY);
            bottom = new Texture2D(im.width, 120);
            bottom.SetPixels(cbottom);
            //bottom.Resize(128, 128);
            bottom.Apply();
            //cbottom = bottom.GetPixels();

            //Find left side of cube map
            Color[] cleft = im.GetPixels(0, 120, 160, 120);
            //cubemap.SetPixels(cleft, CubemapFace.NegativeX);
            left = new Texture2D(160, 120);
            left.SetPixels(cleft);
            //left.Resize(32, 32);
            left.Apply();
            //cleft = left.GetPixels();

            //Find front of cube map
            Color[] cfront = im.GetPixels(160, 120, 160, 240);
            //cubemap.SetPixels(cfront, CubemapFace.PositiveZ);
            front = new Texture2D(160, 240);
            front.SetPixels(cfront);
            //front.Resize(128, 128);
            front.Apply();
            //cfront = front.GetPixels();

            //Find right side of cube map
            Color[] cright = im.GetPixels(320, 120, 160, 240);
            //cubemap.SetPixels(cright, CubemapFace.PositiveX);
            right = new Texture2D(160, 240);
            right.SetPixels(cright);
            //right.Resize(128, 128);
            right.Apply();
            //cright = right.GetPixels();

            //back side set to empty for the moment
            //----
            //----
            //----
            //----

            //Reize textures as squares
            TextureScale.Bilinear(top, pixelSize, pixelSize);
            TextureScale.Bilinear(front, pixelSize, pixelSize);
            TextureScale.Bilinear(bottom, pixelSize, pixelSize);
            TextureScale.Bilinear(right, pixelSize, pixelSize);
            TextureScale.Bilinear(left, pixelSize, pixelSize);

            //Create cubemap using these textures

            front = TexFlipper.Flip(front);

            Color[] facePixels = front.GetPixels();
            cubemap.SetPixels(facePixels, CubemapFace.PositiveZ);
            facePixels = right.GetPixels();
            cubemap.SetPixels(facePixels, CubemapFace.PositiveX);
            facePixels = left.GetPixels();
            cubemap.SetPixels(facePixels, CubemapFace.NegativeX);
            facePixels = top.GetPixels();
            cubemap.SetPixels(facePixels, CubemapFace.PositiveY);
            facePixels = bottom.GetPixels();
            cubemap.SetPixels(facePixels, CubemapFace.NegativeY);
            facePixels = front.GetPixels();
            cubemap.SetPixels(facePixels, CubemapFace.NegativeZ);

            cubemap.Apply();

            GameObject C1 = GameObject.Find("C1");
            Renderer rend = C1.GetComponent<Renderer>();
            //C1.GetComponent<ReflectionProbe>().customBakedTexture = savedCubemap;
            //rend.material.mainTexture = savedCubemap;

            C1.GetComponent<ReflectionProbe>().bakedTexture = cubemap;

            if (Input.GetKeyDown(KeyCode.S))
            {
                //AssetDatabase.CreateAsset(bottom, "Assets/Images/Bottom.mat");
                //AssetDatabase.CreateAsset(top, "Assets/Images/Top.mat");
                //AssetDatabase.CreateAsset(right, "Assets/Images/Right.mat");
                //AssetDatabase.CreateAsset(left, "Assets/Images/Left.mat");
                AssetDatabase.CreateAsset(front, "Assets/Images/Front.mat");

                //AssetDatabase.CreateAsset(cubemap, "Assets/Images/MyCubemap.cubemap");
                AssetDatabase.SaveAssets();
            }
        }

    }
}
